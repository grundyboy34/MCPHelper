package me.aristine.mcphelper;

import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.annotation.Nullable;

import lombok.NonNull;

/**
 * A resource based file (such as in the running code), rather than a disk based file
 * @author Aristine
 */
public class ResourceFile { 
    public static final String SEPERATOR = "/";

    private URI currentURI;
    
    
   
    public ResourceFile() throws NullPointerException {
        this(URI.create(SEPERATOR));
    }
  
    public ResourceFile(@NonNull URI path) throws NullPointerException {
        try {
            URL test = getResource(path.getPath());
            if (test == null && path.getPath().endsWith(SEPERATOR) && path.getPath().length() > 1) {
                path = URI.create(path.getPath().substring(0, path.getPath().length() - 1));
                test = getResource(path.getPath());
                if (test == null) {
                    throw new NullPointerException("Resource null : " + path);
                }
            }
            this.currentURI = path;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Nullable
    public ResourceFile getChild(String resource) {
        ResourceFile child = new ResourceFile(getURI().resolve(resource + SEPERATOR));
        return child != null ? child : null;
    }
    
    @Nullable
    public ResourceFile getParent() throws NullPointerException {
        int index = getURI().getPath().endsWith(SEPERATOR) ? getURI().getPath().lastIndexOf(SEPERATOR, getURI().getPath().length() - 2) : getURI().getPath().lastIndexOf(SEPERATOR);
        ResourceFile parent = new ResourceFile(URI.create(getURI().getPath().substring(0, index) + SEPERATOR));
        return parent != null ? parent : null;
    }
    
    @Nullable
    public InputStream getInputStream() {
        return ResourceFile.class.getResourceAsStream(getURI().getPath());
    }
    
    @Nullable
    public URI getAbsoluteURI() {
        try {
            return getResource(getURI().getPath()).toURI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public URI getURI() {
        return currentURI;
    }
    
    private URL getResource(String path) {
        return ResourceFile.class.getResource(path);
    }
}
